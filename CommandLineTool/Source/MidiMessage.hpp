//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Luke Grayland on 02/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#pragma once
#include <stdio.h>
#include <math.h>

class MidiMessage
{
public:
    MidiMessage();

    MidiMessage(int initNumber, int initVelocity, int channel);

    ~MidiMessage();
    
    void setNoteNumber (int value);

    int getNoteNumber() const;

    void setVelocity(int value);

    int getVelocity () const;

    float getFloatVelocity() const;

    void setChannelNum(int value);

    int getChannelNum() const;

    float getMidiInHertz() const;
    
private:
    int midiNumber;
    int velocity;
    int channelNum;
    
};




//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include <cmath>
#include "MidiMessage.hpp"



int main()
{
    int noteIn;
    
    MidiMessage note(60, 100, 2);
    
    std::cout << "Note number = " << note.getNoteNumber() << "\nFrequency = " << note.getMidiInHertz() << "\nVelocity = " << note.getFloatVelocity() << "\nChannel Number = " << note.getChannelNum() << std::endl;

    
    std::cin >> noteIn;
    
    note.setNoteNumber(noteIn);
    
    std::cout << "Note number = " << note.getNoteNumber() << "\nFrequency = " << note.getMidiInHertz() << "\nVelocity = " << note.getFloatVelocity() << "\nChannel Number = " << note.getChannelNum() << std::endl;
    
    return 0;
}

//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Luke Grayland on 02/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.hpp"

MidiMessage::MidiMessage()
{
    midiNumber = 60;
    velocity = 127.0;
    channelNum = 1;
}
MidiMessage::MidiMessage(int initNumber, int initVelocity, int channel)
{
    midiNumber = initNumber;
    velocity = initVelocity;
    channelNum = channel;
    
}
MidiMessage::~MidiMessage()
{
    
}
void MidiMessage::setNoteNumber (int value)
{
    if (value <= 127 && value >= 0)
    {
        midiNumber = value;
    }
}
int MidiMessage::getNoteNumber() const
{
    return midiNumber;
}
void MidiMessage::setVelocity(int value)
{
    velocity = value;
}
int MidiMessage::getVelocity () const
{
    return velocity;
}
float MidiMessage::getFloatVelocity() const
{
    return velocity / 127.0;
}
void MidiMessage::setChannelNum(int value)
{
    channelNum = value;
}
int MidiMessage::getChannelNum() const
{
    return channelNum;
}
float MidiMessage::getMidiInHertz() const
{
    return 440 * pow(2, (midiNumber-69) / 12.0);
}
